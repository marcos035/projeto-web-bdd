const { I } = inject();

// Add in your custom step files

Given('I an on the login screen', () => {
  // From "features\basic.feature" {"line":14,"column":7}
  I.amOnPage('/')
  I.click('Login')
  I.waitForText('Login',10)
});

When('I fill the email field {string}', (email) => {
  // From "features\basic.feature" {"line":8,"column":7}
  ('#user',email)
});

When('I fill the password field {string}', (password) => {
  // From "features\basic.feature" {"line":16,"column":7}
  I.fillField('#password',password)
});

When('I click on Login', () => {
  // From "features\basic.feature" {"line":17,"column":7}
  I.click('#btnLogin')
});

When('Im see the menssage {string}', (menssageSuccess) => {
  // From "features\basic.feature" {"line":11,"column":7}
  I.see(menssageSuccess,10)
});

When('I fill the email field wrong {string}', (wrongEmail) => {
  // From "features\basic.feature" {"line":15,"column":7}
  ('#user',wrongEmail)
});

When('I see the error menssage {string}', (wrongMessage) => {
  // From "features\basic.feature" {"line":18,"column":7}
  I.see(wrongMessage,10)
});
